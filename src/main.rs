extern crate ldap3;

use ldap3::{LdapConn};

fn main() {
    // Création de la connexion au serveur LDAP
    let mut conn = LdapConn::new("ldap://ldap.example.com:389").unwrap_or_else(|e| {
        println!("Echec de la connexion au serveur LDAP : {}", e);
        std::process::exit(1);
    });

    // Authentification sur le serveur LDAP
    //let bind_result = conn.simple_bind("cn=admin,dc=example,dc=com", "password").unwrap();
    let bind_result = conn.simple_bind("cn=admin,dc=example,dc=com", "P@ssw0rd").unwrap().success();
    if bind_result.is_ok() {
        println!("Connexion établie avec succès !");
    } else {
        println!("Echec de la connexion !");
    }
}
